module Gitlab
  module GceNode
    # Values as of 2020-03-09
    # https://cloud.google.com/compute/docs/disks/performance
    def self.disk_performance(node)
      disk_perf = {}
      cpus = node['cpu']['total']
      node['block_device'].each do |disk, p|
        next unless p['vendor'] == 'Google'
        disk_size_gb = p['logical_block_size'].to_f * p['size'].to_f / 1024 / 1024 / 1024
        if p['rotational'] == '1'
          disk_read_iops = disk_size_gb * node['node_exporter']['gce_disk_perf']['read_iops_per_gb_standard']
          disk_write_iops = disk_size_gb * node['node_exporter']['gce_disk_perf']['write_iops_per_gb_standard']
          disk_mb = disk_size_gb * node['node_exporter']['gce_disk_perf']['mb_per_gb_standard']
          disk_read_mb = disk_mb
          disk_write_mb = [disk_mb, 400].min
        else
          disk_read_iops = disk_size_gb * node['node_exporter']['gce_disk_perf']['iops_per_gb_ssd']
          disk_write_iops = disk_read_iops
          disk_mb = disk_size_gb * node['node_exporter']['gce_disk_perf']['mb_per_gb_ssd']
          case cpus
          when 1..3
            disk_read_iops = [disk_read_iops, 15000].min
            disk_write_iops = [disk_write_iops, 9000].min
            disk_read_mb = [disk_mb, 240].min
            disk_write_mb = [disk_mb, 72 * cpus].min
          when 4..7
            disk_read_iops = [disk_read_iops, 25000].min
            disk_write_iops = disk_read_iops
            disk_read_mb = [disk_mb, 240].min
            disk_write_mb = [disk_mb, 240].min
          when 8..15
            disk_read_iops = [disk_read_iops, 25000].min
            disk_write_iops = disk_read_iops
            disk_read_mb = [disk_mb, 800].min
            disk_write_mb = [disk_mb, 400].min
          when 16..31
            disk_read_iops = [disk_read_iops, 25000].min
            disk_write_iops = disk_read_iops
            disk_read_mb = [disk_mb, 1200].min
            disk_write_mb = [disk_mb, 800].min
          when 32.63
            disk_read_iops = [disk_read_iops, 60000].min
            disk_write_iops = [disk_write_iops, 30000].min
            disk_read_mb = [disk_mb, 1200].min
            disk_write_mb = [disk_mb, 800].min
          else
            disk_read_iops = [disk_read_iops, 100000].min
            disk_write_iops = [disk_write_iops, 30000].min
            disk_read_mb = [disk_mb, 1200].min
            disk_write_mb = [disk_mb, 800].min
          end
        end
        disk_perf[disk] = {
          'max_read_iops': disk_read_iops,
          'max_write_iops': disk_write_iops,
          'max_read_bytes_sec': disk_read_mb * 1024 * 1024,
          'max_write_bytes_sec': disk_write_mb * 1024 * 1024,
        }
      end
      disk_perf
    end
  end
end

Chef::DSL::Recipe.include Gitlab::GceNode
Chef::Resource.include Gitlab::GceNode
Chef::DSL::Recipe.include Gitlab::GceNode
