default['influxdb_exporter']['checksum']    = '75d56278c78dc6559550c2da7fcd6272b65418b13e31562292cc97379023329d'
default['influxdb_exporter']['dir']         = '/opt/prometheus/influxdb_exporter'
default['influxdb_exporter']['binary']      = "#{node['influxdb_exporter']['dir']}/influxdb_exporter"
default['influxdb_exporter']['log_dir']     = '/var/log/prometheus/influxdb_exporter'
default['influxdb_exporter']['version']     = '0.2.0'
default['influxdb_exporter']['binary_url']  = "https://github.com/prometheus/influxdb_exporter/releases/download/v#{node['influxdb_exporter']['version']}/influxdb_exporter-#{node['influxdb_exporter']['version']}.linux-amd64.tar.gz"

# You can add any flags to the runit script by adding another flag attribute
# default["influxdb_exporter"]["flags"]["your.config.option"] = "value"
# Please see the docs at https://github.com/prometheus/influxdb_exporter

default['influxdb_exporter']['flags']['web.listen-address'] = '0.0.0.0:9122'
