#
# Cookbook:: GitLab::Monitoring
# Attributes:: consul_exporter
#
default['consul_exporter']['dir'] = '/opt/prometheus/consul_exporter'
default['consul_exporter']['log_dir'] = '/var/log/prometheus/consul_exporter'
default['consul_exporter']['checksum'] = '1457e50356ae28e510d4ac927a916938bf61ac87026facc20289f868518b36af'
default['consul_exporter']['version'] = '0.8.0'
default['consul_exporter']['binary_url'] = "https://github.com/prometheus/consul_exporter/releases/download/v#{node['consul_exporter']['version']}/consul_exporter-#{node['consul_exporter']['version']}.linux-amd64.tar.gz"
default['consul_exporter']['flags']['web.listen-address'] = ':9107'
