default['statsd_exporter']['checksum']    = 'c9e685db2558b96d40bb60c36420054457421fce8c6aad4b7fa14d9cff0ff04d'
default['statsd_exporter']['version']     = '0.14.1'
default['statsd_exporter']['binary_url']  = "https://github.com/prometheus/statsd_exporter/releases/download/v#{node['statsd_exporter']['version']}/statsd_exporter-#{node['statsd_exporter']['version']}.linux-amd64.tar.gz"
default['statsd_exporter']['dir']         = '/opt/prometheus/statsd_exporter'
default['statsd_exporter']['log_dir']     = '/var/log/prometheus/statsd_exporter'
default['statsd_exporter']['binary']      = "#{node['statsd_exporter']['dir']}/statsd_exporter"
default['statsd_exporter']['mapping_file'] = "#{node['statsd_exporter']['dir']}/statsd_mappings.yml"
default['statsd_exporter']['port'] = 9102

default['statsd_exporter']['flags']['web.listen-address'] = ":#{node['statsd_exporter']['port']}"
default['statsd_exporter']['flags']['statsd.mapping-config'] = node['statsd_exporter']['mapping_file']

default['statsd_exporter']['defaults']['buckets'] = [0.005, 0.01, 0.025, 0.05, 0.1, 0.25, 0.5, 1.0, 2.5, 10.0, 25.0 ]
default['statsd_exporter']['defaults']['match_type'] = 'glob'
default['statsd_exporter']['defaults']['timer_type'] = 'histogram'

default['statsd_exporter']['mappings'] = []
