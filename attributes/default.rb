#
# Cookbook:: GitLab::Monitoring
# Attributes:: default
#

default['prometheus']['user']        = 'prometheus'
default['prometheus']['group']       = 'prometheus'
