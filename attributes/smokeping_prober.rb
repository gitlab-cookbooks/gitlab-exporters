default['smokeping_prober']['checksum']    = '3ff505954e48979289732327e0f33703a6c02ee331e97fe958b52b8faf0ae6d8'
default['smokeping_prober']['version']     = '0.3.0'
default['smokeping_prober']['binary_url']  = "https://github.com/SuperQ/smokeping_prober/releases/download/v#{node['smokeping_prober']['version']}/smokeping_prober-#{node['smokeping_prober']['version']}.linux-amd64.tar.gz"
default['smokeping_prober']['dir']         = '/opt/prometheus/smokeping_prober'
default['smokeping_prober']['log_dir']     = '/var/log/prometheus/smokeping_prober'
default['smokeping_prober']['binary']      = "#{node['smokeping_prober']['dir']}/smokeping_prober"

default['smokeping_prober']['flags']['ping.interval'] = '1s'
# Use ICMP, requires cap_net_raw=+ep
default['smokeping_prober']['flags']['privileged'] = true
default['smokeping_prober']['flags']['web.listen-address'] = ':9374'

default['smokeping_prober']['targets'] = ['127.0.0.1']
