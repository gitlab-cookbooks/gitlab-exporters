default['stackdriver_exporter']['checksum']    = 'e454b260272a231228f58174f5ad042c56548d3e11a63f612ebb1c65be938b2e'
default['stackdriver_exporter']['dir']         = '/opt/prometheus/stackdriver_exporter'
default['stackdriver_exporter']['binary']      = "#{node['stackdriver_exporter']['dir']}/stackdriver_exporter"
default['stackdriver_exporter']['log_dir']     = '/var/log/prometheus/stackdriver_exporter'
default['stackdriver_exporter']['version']     = '0.11.0'
default['stackdriver_exporter']['binary_url']  = "https://github.com/prometheus-community/stackdriver_exporter/releases/download/v#{node['stackdriver_exporter']['version']}/stackdriver_exporter-#{node['stackdriver_exporter']['version']}.linux-amd64.tar.gz"

# You can add any flags to the runit script by adding another flag attribute
# default["stackdriver_exporter"]["flags"]["your.config.option"] = "value"
# Please see the docs at https://github.com/prometheus/stackdriver_exporter
default['stackdriver_exporter']['flags']['google.project-id'] = 'gitlab-staging-1'

# See https://cloud.google.com/monitoring/api/metrics_gcp for available metrics
# Redis cannot be included because of https://github.com/frodenas/stackdriver_exporter/issues/36

default['stackdriver_exporter']['metrics_prefixes'] = [
  'cloudsql.googleapis.com/database/cpu',
  'cloudsql.googleapis.com/database/disk',
  'cloudsql.googleapis.com/database/instance_state',
  'cloudsql.googleapis.com/database/memory',
  'cloudsql.googleapis.com/database/mysql',
  'cloudsql.googleapis.com/database/postgresql/transaction_count',
  'cloudsql.googleapis.com/database/state',
  'cloudsql.googleapis.com/database/up',
  'compute.googleapis.com/firewall',
  'compute.googleapis.com/instance/disk/throttled_read_',
  'compute.googleapis.com/instance/disk/throttled_write_',
  'compute.googleapis.com/instance/integrity',
  'compute.googleapis.com/instance/uptime', # Used to alert on GKE instance limit saturation.
  'compute.googleapis.com/mirroring',
  'compute.googleapis.com/nat',
  'container.googleapis.com',
  'file.googleapis.com',
  'loadbalancing.googleapis.com/https/backend_latencies',
  'loadbalancing.googleapis.com/https/backend_request_',
  'loadbalancing.googleapis.com/https/backend_response_',
  'loadbalancing.googleapis.com/https/request_',
  'loadbalancing.googleapis.com/https/response_',
  'loadbalancing.googleapis.com/l3/external/egress_',
  'loadbalancing.googleapis.com/l3/external/ingress_',
  'loadbalancing.googleapis.com/l3/internal/egress_',
  'loadbalancing.googleapis.com/l3/internal/ingress_',
  'logging.googleapis.com',
  'monitoring.googleapis.com',
  'pubsub.googleapis.com/topic/byte_cost',
  'pubsub.googleapis.com/topic/send_message_operation_count',
  'pubsub.googleapis.com/subscription',
  'router.googleapis.com/nat',
  'storage.googleapis.com',
  'vpn.googleapis.com',
]

default['stackdriver_exporter']['flags']['monitoring.metrics-type-prefixes'] = node['stackdriver_exporter']['metrics_prefixes'].sort.uniq.join(',')
default['stackdriver_exporter']['flags']['stackdriver.http-timeout'] = '40s'
