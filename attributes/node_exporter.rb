default['node_exporter']['checksum']    = 'af999fd31ab54ed3a34b9f0b10c28e9acee9ef5ac5a5d5edfdde85437db7acbb'
default['node_exporter']['dir']         = '/opt/prometheus/node_exporter'
default['node_exporter']['scripts_dir'] = "#{node['node_exporter']['dir']}/scripts"
default['node_exporter']['binary']      = "#{node['node_exporter']['dir']}/node_exporter"
default['node_exporter']['log_dir']     = '/var/log/prometheus/node_exporter'
default['node_exporter']['version']     = '1.5.0'
default['node_exporter']['binary_url']  = "https://github.com/prometheus/node_exporter/releases/download/v#{node['node_exporter']['version']}/node_exporter-#{node['node_exporter']['version']}.linux-amd64.tar.gz"
default['node_exporter']['port']        = 9100

# You can add any flags to the runit script by adding another flag attribute
# default["node_exporter"]["flags"]["your.config.option"] = "value"
# Please see the docs at https://github.com/prometheus/node_exporter#collectors

default['node_exporter']['flags']['web.listen-address'] = ":#{node['node_exporter']['port']}"

# Enable extra collectors.
default['node_exporter']['flags']['collector.mountstats'] = true
default['node_exporter']['flags']['collector.nfs']        = true
default['node_exporter']['flags']['collector.ntp']        = true

# Extra collector flags.
default['node_exporter']['flags']['collector.textfile.directory'] = "#{node['node_exporter']['dir']}/metrics"

# This adds `nfs.*` to the default node_exporter 1.0.x list.
# ^(autofs|binfmt_misc|bpf|cgroup2?|configfs|debugfs|devpts|devtmpfs|fusectl|hugetlbfs|iso9660|mqueue|nfs.*|nsfs|overlay|proc|procfs|pstore|rpc_pipefs|securityfs|selinuxfs|squashfs|sysfs|tracefs)$
default['node_exporter']['flags']['collector.filesystem.ignored-fs-types'] = '"^(autofs|binfmt_misc|bpf|cgroup2?|configfs|debugfs|devpts|devtmpfs|fusectl|hugetlbfs|iso9660|mqueue|nfs.*|nsfs|overlay|proc|procfs|pstore|rpc_pipefs|securityfs|selinuxfs|squashfs|sysfs|tracefs)$"'

# This adds Tcp: `OutRsts` from /proc/net/snmp to the default node_exporter
# 1.0.x list.
default['node_exporter']['flags']['collector.netstat.fields'] = '"^(.*_(InErrors|InErrs)|Ip_Forwarding|Ip(6|Ext)_(InOctets|OutOctets)|Icmp6?_(InMsgs|OutMsgs)|TcpExt_(Listen.*|Syncookies.*|TCPSynRetrans)|Tcp_(ActiveOpens|InSegs|OutSegs|OutRsts|PassiveOpens|RetransSegs|CurrEstab)|Udp6?_(InDatagrams|OutDatagrams|NoPorts|RcvbufErrors|SndbufErrors))$"'

default['node_exporter']['ntpd_metrics_logdir'] = '/var/log/prometheus/node_exporter_ntpd_metrics'
default['node_exporter']['apt_metrics_logdir'] = '/var/log/prometheus/node_exporter_apt_metrics'

# Values as of 2020-03-09
# https://cloud.google.com/compute/docs/disks/performance
default['node_exporter']['gce_disk_perf']['iops_per_gb_ssd'] = 30.0
default['node_exporter']['gce_disk_perf']['read_iops_per_gb_standard'] = 0.75
default['node_exporter']['gce_disk_perf']['write_iops_per_gb_standard'] = 1.5
default['node_exporter']['gce_disk_perf']['mb_per_gb_ssd'] = 0.48
default['node_exporter']['gce_disk_perf']['mb_per_gb_standard'] = 0.12
