default['process_exporter']['checksum']    = '07df6b120e46fde6bf15588ee293f6303d07c9c8da4b814b93deba901e621750'
default['process_exporter']['version']     = '0.7.3'
default['process_exporter']['package_url'] = "https://github.com/ncabatoff/process-exporter/releases/download/v#{node['process_exporter']['version']}/process-exporter_#{node['process_exporter']['version']}_linux_amd64.deb"
default['process_exporter']['configfile'] = '/etc/process-exporter/chef-configured.yaml'
default['process_exporter']['listen-address'] = ':9256'
default['process_exporter']['collect_threads'] = 'false'
default['process_exporter']['recheck'] = 'false'

# If this is empty the recipe will not do anything.
default['process_exporter']['config'] = {}
