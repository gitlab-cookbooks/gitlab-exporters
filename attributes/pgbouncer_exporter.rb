#
# Cookbook:: GitLab::Monitoring
# Attributes:: pgbouncer_exporter
#
default['pgbouncer_exporter']['dir'] = '/opt/prometheus/pgbouncer_exporter'
default['pgbouncer_exporter']['log_dir'] = '/var/log/prometheus/pgbouncer_exporter'
default['pgbouncer_exporter']['checksum'] = 'db0ff24e5ed54e10181b77579e407d3dd86f3a4e8e46de05e09b098947f80f64'
default['pgbouncer_exporter']['version'] = '0.4.0'
default['pgbouncer_exporter']['user'] = 'gitlab-psql'
default['pgbouncer_exporter']['binary_url'] = "https://github.com/prometheus-community/pgbouncer_exporter/releases/download/v#{node['pgbouncer_exporter']['version']}/pgbouncer_exporter-#{node['pgbouncer_exporter']['version']}.linux-amd64.tar.gz"

default['pgbouncer_exporter']['flags']['log.format'] = 'json'
default['pgbouncer_exporter']['flags']['pgBouncer.connectionString'] = "'user=pgbouncer dbname=pgbouncer sslmode=disable port=6432 host=/tmp'"
default['pgbouncer_exporter']['flags']['web.listen-address'] = '0.0.0.0:9188'
default['pgbouncer_exporter']['secrets']['backend'] = 'chef_vault'
default['pgbouncer_exporter']['secrets']['path'] = 'pgbouncer-exporter'

default['pgbouncer_exporter']['instances'] = [] # [{"flags" => {}, "log_dir" => ""}]
