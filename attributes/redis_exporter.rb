default['redis_exporter']['dir'] = '/opt/prometheus/redis_exporter'
default['redis_exporter']['log_dir'] = '/var/log/prometheus/redis_exporter'
default['redis_exporter']['version'] = '1.45.0'
default['redis_exporter']['binary_url'] = "https://github.com/oliver006/redis_exporter/releases/download/v#{node['redis_exporter']['version']}/redis_exporter-v#{node['redis_exporter']['version']}.linux-amd64.tar.gz"
default['redis_exporter']['checksum'] = '0890f4a75c41a953b608a3c025ef735296a473e0119ed31864c8510efe7a8393'
default['redis_exporter']['env'] = {}
default['redis_exporter']['systemd'] = false
default['redis_exporter']['cluster_name'] = 'default'
default['redis_exporter']['omnibus_secrets'] = true
default['redis_exporter']['secrets']['backend'] = 'chef_vault'
default['redis_exporter']['secrets']['path'] = 'gitlab-cluster-base'
default['redis_exporter']['monitor_sentinel'] = false
default['redis_exporter']['gitlab_metric_file'] = '/opt/prometheus/node_exporter/metrics/redis_sentinel_client_info.prom'
