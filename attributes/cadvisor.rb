default['cadvisor']['sha256sum'] = '0f55721088a6f758b84888bbbc9dd8497c13ba45aeb5a592ace676e887928fc3'
default['cadvisor']['version'] = '0.46.0'
default['cadvisor']['binary_url'] = "https://github.com/google/cadvisor/releases/download/v#{node['cadvisor']['version']}/cadvisor-v#{node['cadvisor']['version']}-linux-amd64"
default['cadvisor']['dir'] = '/opt/prometheus/cadvisor'
default['cadvisor']['binary'] = "#{node['cadvisor']['dir']}/cadvisor"
default['cadvisor']['port'] = 9101
default['cadvisor']['flags']['port'] = "#{node['cadvisor']['port']}"
default['cadvisor']['flags']['disable_metrics'] = 'percpu,cpu_topology,cpuLoad,cpuset,network,advtcp,tcp,udp,diskIO,disk,sched,process,referenced_memory,hugetlb,memory_numa,resctrl,perf_event,accelerator'
