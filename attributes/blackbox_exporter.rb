#
# Cookbook:: GitLab::Monitoring
# Attributes:: blackbox_exporter
#

default['blackbox_exporter']['dir']         = '/opt/prometheus/blackbox_exporter'
default['blackbox_exporter']['binary']      = "#{node['blackbox_exporter']['dir']}/blackbox_exporter"
default['blackbox_exporter']['log_dir']     = '/var/log/prometheus/blackbox_exporter'

default['blackbox_exporter']['version']     = '0.17.0'
default['blackbox_exporter']['checksum']    = '6ebe26d1e97d26ee08d0cd6d37a34f2f67c8414ea5e40407eadc0ac950ed518e'
default['blackbox_exporter']['binary_url']  = "https://github.com/prometheus/blackbox_exporter/releases/download/v#{node['blackbox_exporter']['version']}/blackbox_exporter-#{node['blackbox_exporter']['version']}.linux-amd64.tar.gz"
default['blackbox_exporter']['port'] = 9115

default['blackbox_exporter']['flags']['config.file']        = "#{node['blackbox_exporter']['dir']}/blackbox_exporter.yml"
default['blackbox_exporter']['flags']['web.listen-address'] = ":#{node['blackbox_exporter']['port']}"

default['blackbox_exporter']['modules']['http_2xx']['prober']         = 'http'

default['blackbox_exporter']['modules']['http_post_2xx']['prober']    = 'http'
default['blackbox_exporter']['modules']['http_post_2xx']['http_method'] = 'POST'

default['blackbox_exporter']['modules']['http_head_2xx']['prober'] = 'http'
default['blackbox_exporter']['modules']['http_head_2xx']['http_method'] = 'HEAD'

default['blackbox_exporter']['modules']['http_2xx_custom_ua']['headers'] = { 'user-agent' => 'gitlab-blackbox-probe' }

default['blackbox_exporter']['modules']['http_dev_gitlab_org_2xx']['prober'] = 'http'
default['blackbox_exporter']['modules']['http_dev_gitlab_org_2xx']['http_method'] = 'GET'

default['blackbox_exporter']['modules']['http_gitlab_com_auth_2xx']['prober'] = 'http'
default['blackbox_exporter']['modules']['http_gitlab_com_auth_2xx']['http_method'] = 'GET'

default['blackbox_exporter']['modules']['elastic']['prober'] = 'http'
default['blackbox_exporter']['modules']['elastic']['http_method'] = 'GET'
default['blackbox_exporter']['modules']['elastic']['ilm_error_regex'] = '"step":"ERROR"'
default['blackbox_exporter']['modules']['elastic']['ilm_status_stopped_regex'] = '"operation_mode":"STOPPED"'
default['blackbox_exporter']['modules']['elastic']['cluster_health_regex'] = '"status":"green"'

default['blackbox_exporter']['modules']['tcp_connect']['prober']        = 'tcp'
default['blackbox_exporter']['modules']['tcp_connect']['timeout']       = '5s'

default['blackbox_exporter']['modules']['tls_expiry']['prober']         = 'tcp'
default['blackbox_exporter']['modules']['tls_expiry']['tcp_tls']        = 'true'
default['blackbox_exporter']['modules']['tls_expiry']['tcp_tls_config'] = { 'insecure_skip_verify' => false }

default['blackbox_exporter']['modules']['pop3s_banner']['prober']       = 'tcp'
default['blackbox_exporter']['modules']['pop3s_banner']['tcp_query_response'] = { 'expect' => '^+OK' }
default['blackbox_exporter']['modules']['pop3s_banner']['tcp_tls'] = 'true'
default['blackbox_exporter']['modules']['pop3s_banner']['tcp_tls_config'] = { 'insecure_skip_verify' => false }
default['blackbox_exporter']['modules']['icmp']['prober']               = 'icmp'
default['blackbox_exporter']['modules']['icmp']['timeout']              = '5s'
default['blackbox_exporter']['secrets']['backend'] = 'chef_vault'
default['blackbox_exporter']['secrets']['path'] = 'blackbox_exporter'
