default['haproxy_exporter']['checksum']    = '41d797bc7d54023ea0ea23e58be5795a0947289d3d33beab3372c52e4fcbb298'
default['haproxy_exporter']['dir']         = '/opt/prometheus/haproxy_exporter'
default['haproxy_exporter']['binary']      = "#{node['haproxy_exporter']['dir']}/haproxy_exporter"
default['haproxy_exporter']['log_dir']     = '/var/log/prometheus/haproxy_exporter'
default['haproxy_exporter']['version']     = '0.12.0'
default['haproxy_exporter']['binary_url']  = "https://github.com/prometheus/haproxy_exporter/releases/download/v#{node['haproxy_exporter']['version']}/haproxy_exporter-#{node['haproxy_exporter']['version']}.linux-amd64.tar.gz"
default['haproxy_exporter']['haproxy_group'] = 'haproxy'

# You can add any flags to the runit script by adding another flag attribute
# default["haproxy_exporter"]["flags"]["your.config.option"] = "value"
# Please see the docs at https://github.com/prometheus/haproxy_exporter
default['haproxy_exporter']['flags']['haproxy.scrape-uri'] = 'unix:/run/haproxy/admin.sock'
default['haproxy_exporter']['flags']['haproxy.pid-file']   = '/run/haproxy.pid'
default['haproxy_exporter']['flags']['log.format']         = 'json'
