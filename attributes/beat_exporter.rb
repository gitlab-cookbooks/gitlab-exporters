
default['beat_exporter']['dir'] = '/opt/prometheus/beat_exporter'
default['beat_exporter']['version'] = '0.3.1'
default['beat_exporter']['binary_url'] = "https://github.com/trustpilot/beat-exporter/releases/download/#{node['beat_exporter']['version']}/beat-exporter-#{node['beat_exporter']['version']}-linux-amd64.tar.gz"
default['beat_exporter']['checksum'] = '9d97a5b4e9e1ac0ff80901f105108b72c4f609201cda518bece26a8ab7081de5'
default['beat_exporter']['port'] = 9479
