resource_name :blackbox_module
provides :blackbox_module

# General module options.
property :prober,                String, required: true
property :timeout,               String, default: '59s'
property :preferred_ip_protocol, String, default: 'ip4'
property :config_file,           String, default: lazy { node['blackbox_exporter']['flags']['config.file'] }

# HTTP prober options
property :http_method,                          String
property :http_valid_status_codes,              Array
property :http_no_follow_redirects,             [true, false]
property :http_fail_if_ssl,                     [true, false]
property :http_fail_if_not_ssl,                 [true, false]
property :http_fail_if_body_matches_regexp,     Array
property :http_fail_if_body_not_matches_regexp, Array
property :http_headers,                         Hash

# TCP prober options.
property :tcp_query_response, Hash
property :tcp_tls_config,     Hash
property :tcp_tls,            String, default: 'false'

default_action :create

action :create do
  with_run_context :root do
    edit_resource(:template, new_resource.config_file) do |new_resource|
      variables[:modules] ||= {}
      variables[:modules][new_resource.name] ||= {}
      variables[:modules][new_resource.name]['prober'] = new_resource.prober
      variables[:modules][new_resource.name]['timeout'] = new_resource.timeout

      conf = {}
      case new_resource.prober
      when 'http'
        conf['preferred_ip_protocol'] = new_resource.preferred_ip_protocol
        conf['method'] = new_resource.http_method
        conf['no_follow_redirects'] = new_resource.http_no_follow_redirects
        conf['fail_if_ssl'] = new_resource.http_fail_if_ssl
        conf['fail_if_not_ssl'] = new_resource.http_fail_if_not_ssl

        if new_resource.http_valid_status_codes
          conf['valid_status_codes'] = new_resource.http_valid_status_codes.sort
        end

        if new_resource.http_fail_if_body_matches_regexp
          conf['fail_if_body_matches_regexp'] = new_resource.http_fail_if_body_matches_regexp.sort
        end

        if new_resource.http_fail_if_body_not_matches_regexp
          conf['fail_if_body_not_matches_regexp'] = new_resource.http_fail_if_body_not_matches_regexp.sort
        end

        conf['headers'] = new_resource.http_headers

        variables[:modules][new_resource.name]['http'] = conf.delete_if { |_key, value| value.nil? }
      when 'tcp'
        conf['preferred_ip_protocol'] = new_resource.preferred_ip_protocol

        if new_resource.tcp_query_response
          # transform each key/value pair to its own entry
          conf['query_response'] = new_resource.tcp_query_response.map { |k, v| { k => v } }
        end

        if new_resource.tcp_tls
          conf['tls'] = new_resource.tcp_tls.to_s == 'true' # transform to boolean
        end

        if new_resource.tcp_tls_config
          conf['tls_config'] = new_resource.tcp_tls_config.to_hash
        end

        variables[:modules][new_resource.name]['tcp'] = conf.delete_if { |_key, value| value.nil? }
      end

      action :nothing
      delayed_action :create

      not_if { node['prometheus']['allow_external_config'] && File.exist?(node['prometheus']['flags']['config.file']) }
    end
  end
end

action :delete do
  template config_file do
    action :delete
  end
end
