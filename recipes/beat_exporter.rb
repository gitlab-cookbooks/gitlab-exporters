include_recipe 'ark::default'

%w( curl tar ).each do |pkg|
  package pkg
end

ark 'beat-exporter' do
  url node['beat_exporter']['binary_url']
  checksum node['beat_exporter']['checksum']
  version node['beat_exporter']['version']
  prefix_root Chef::Config['file_cache_path']
  path node['beat_exporter']['dir']
  strip_components 0
  owner node['prometheus']['user']
  group node['prometheus']['group']
  creates 'beat-exporter'
  action :dump
  notifies :restart, 'systemd_unit[beat_exporter.service]'
end

beat_exporter_unit = {
  Unit: {
    Description: 'beat_exporter',
    Documentation: ['https://github.com/trustpilot/beat-exporter'],
    After: 'network.target',
  },
  Service: {
    Type: 'simple',
    ExecStart: "#{node['beat_exporter']['dir']}/beat-exporter -web.listen-address :#{node['beat_exporter']['port']}",
    ExecReload: '/bin/kill -HUP $MAINPID',
    KillMode: 'process',
    LimitNOFILE: '10000',
    Restart: 'always',
    RestartSec: '5s',
    User: node['prometheus']['user'],
  },
  Install: {
    WantedBy: 'multi-user.target',
  },
}

systemd_unit 'beat_exporter.service' do
  content beat_exporter_unit
  action [:create, :enable, :start]
end

# Consul registration

if !node['gitlab_consul'].nil? && node['gitlab_consul']['agent']['enabled'] == true
  port = node['beat_exporter']['port']
  consul_definition 'beat_exporter' do
    type 'service'
    parameters(
      check: {
        http: "http://127.0.0.1:#{port}/",
        interval: '15s',
      },
      name: 'beat_exporter',
      port: port.to_i,
      tags: [
        'metrics',
      ]
    )
    notifies :reload, 'consul_service[consul]', :delayed
  end
end
