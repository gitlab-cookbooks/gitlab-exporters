include_recipe 'gitlab-exporters::default'

postgres_pw = get_secrets(node['postgres_exporter']['secrets']['backend'],
                          node['postgres_exporter']['secrets']['path'],
                          node['postgres_exporter']['secrets']['key'])['exporter_user_password']

directory node['postgres_exporter']['dir'] do
  owner node['prometheus']['user']
  group node['prometheus']['group']
  mode '0755'
  recursive true
end

directory node['postgres_exporter']['log_dir'] do
  owner node['prometheus']['user']
  group node['prometheus']['group']
  mode '0755'
  recursive true
end

include_recipe 'ark::default'

%w( curl tar bzip2 ).each do |pkg|
  package pkg
end

dir_name = ::File.basename(node['postgres_exporter']['dir'])
dir_path = ::File.dirname(node['postgres_exporter']['dir'])

ark dir_name do
  url node['postgres_exporter']['binary_url']
  checksum node['postgres_exporter']['checksum']
  version node['postgres_exporter']['version']
  prefix_root Chef::Config['file_cache_path']
  path dir_path
  owner node['prometheus']['user']
  group node['prometheus']['group']
  action :put
  notifies :restart, 'runit_service[postgres_exporter]'
end

template "#{node['postgres_exporter']['dir']}/queries.yaml" do
  source 'postgres_exporter/queries.yaml.erb'
  group node['prometheus']['group']
  owner node['prometheus']['user']
  mode '0644'
  notifies :reload, 'runit_service[postgres_exporter]'
end

include_recipe 'runit::default'
runit_service 'postgres_exporter' do
  options(
    username: node['postgres_exporter']['db_user'],
    password: postgres_pw
  )
  default_logger true
  log_dir node['postgres_exporter']['log_dir']
end
