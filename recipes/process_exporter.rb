return if node['process_exporter']['config'].empty?

deb_tmp = "#{Chef::Config['file_cache_path']}/process_exporter.deb"

service 'process-exporter' do
  action :nothing
  reload_command 'systemctl daemon-reload'
end

remote_file 'process_exporter.deb' do
  source node['process_exporter']['package_url']
  path deb_tmp
  use_conditional_get true
  checksum node['process_exporter']['checksum']
end

dpkg_package 'process-exporter' do
  source deb_tmp
  version node['process_exporter']['version']
  action :install
  notifies :enable, 'service[process-exporter]', :immediately
  notifies :start, 'service[process-exporter]', :delayed
end

file 'process-exporter config' do
  path node['process_exporter']['configfile']
  owner 'root'
  group 'root'
  mode '0644'
  content lazy { YAML.dump(node['process_exporter']['config'].to_hash) }
  notifies :restart, 'service[process-exporter]', :delayed
end

directory '/etc/systemd/system/process-exporter.service.d' do
  owner 'root'
  group 'root'
  recursive true
  action :create
end

template 'process-exporter systemd override' do
  path '/etc/systemd/system/process-exporter.service.d/override.conf'
  source 'process-exporter.override.conf.erb'
  owner 'root'
  group 'root'
  mode '0644'
  variables(
    configfile: node['process_exporter']['configfile'],
    listen_address: node['process_exporter']['listen-address'],
    collect_threads: node['process_exporter']['collect_threads'],
    recheck: node['process_exporter']['recheck']
  )
  notifies :reload, 'service[process-exporter]', :immediately # will reload systemd units as per line 6
  notifies :restart, 'service[process-exporter]', :delayed
end
