require 'yaml'
include_recipe 'gitlab-exporters::default'

directory node['node_exporter']['dir'] do
  owner node['prometheus']['user']
  group node['prometheus']['group']
  mode '0755'
  recursive true
end

textfile_dir = node['node_exporter']['flags']['collector.textfile.directory']

directory textfile_dir do
  owner node['prometheus']['user']
  group node['prometheus']['group']
  mode '0755'
  recursive true
end

include_recipe 'gitlab-exporters::chef_client'

directory node['node_exporter']['log_dir'] do
  owner node['prometheus']['user']
  group node['prometheus']['group']
  mode '0755'
  recursive true
end

directory node['node_exporter']['scripts_dir'] do
  owner node['prometheus']['user']
  group node['prometheus']['group']
  mode '0755'
  recursive true
end

include_recipe 'ark::default'

%w( curl tar bzip2 ).each do |pkg|
  package pkg
end

dir_name = ::File.basename(node['node_exporter']['dir'])
dir_path = ::File.dirname(node['node_exporter']['dir'])

ark dir_name do
  url node['node_exporter']['binary_url']
  checksum node['node_exporter']['checksum']
  version node['node_exporter']['version']
  prefix_root Chef::Config['file_cache_path']
  path dir_path
  owner node['prometheus']['user']
  group node['prometheus']['group']
  action :put
  notifies :restart, 'runit_service[node_exporter]'
end

include_recipe 'runit::default'
runit_service 'node_exporter' do
  default_logger true
  log_dir node['node_exporter']['log_dir']
end

template File.join(textfile_dir, 'gce_disk_io_quota.prom') do
  source 'gce_disk_io_quota.prom.erb'
  variables(
    disks: Gitlab::GceNode.disk_performance(node)
  )
  owner node['prometheus']['user']
  group node['prometheus']['group']
  mode '0644'
end

# For `sponge` used by textfile collectors.
package 'moreutils'

###
# NTPd metrics textfile collector
cookbook_file "#{node['node_exporter']['dir']}/ntpd_metrics.py" do
  source 'ntpd_metrics.py'
  owner node['prometheus']['user']
  group node['prometheus']['group']
  mode '0755'
  notifies :restart, 'runit_service[ntpd_metrics]', :delayed
end

directory node['node_exporter']['ntpd_metrics_logdir'] do
  owner node['prometheus']['user']
  group node['prometheus']['group']
  mode '0755'
  recursive true
end

runit_service 'ntpd_metrics' do
  options(metrics_textfile: "#{textfile_dir}/ntpd_metrics.prom")
  default_logger true
  log_dir node['node_exporter']['ntpd_metrics_logdir']
  ignore_failure true
end

###
# apt-get upgrade textfile collector
cookbook_file "#{node['node_exporter']['dir']}/apt.sh" do
  source 'apt.sh'
  owner node['prometheus']['user']
  group node['prometheus']['group']
  mode '0755'
  notifies :restart, 'runit_service[apt_metrics]', :delayed
end

directory node['node_exporter']['apt_metrics_logdir'] do
  owner node['prometheus']['user']
  group node['prometheus']['group']
  mode '0755'
  recursive true
end

runit_service 'apt_metrics' do
  options(metrics_textfile: "#{textfile_dir}/apt_metrics.prom")
  default_logger true
  log_dir node['node_exporter']['apt_metrics_logdir']
  ignore_failure true
end

# Consul registration

if !node['gitlab_consul'].nil? && node['gitlab_consul']['agent']['enabled'] == true
  port = node['node_exporter']['port']
  consul_definition 'node' do
    type 'service'
    parameters(
      check: {
        http: "http://127.0.0.1:#{port}/",
        interval: '15s',
      },
      name: 'node',
      port: port.to_i,
      tags: [
        'metrics',
      ]
    )
    notifies :reload, 'consul_service[consul]', :delayed
  end
end
