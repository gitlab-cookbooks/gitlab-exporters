# Configure default secrets based on environment in GCE.
env = node.chef_environment

if !node['cloud'].nil? && node['cloud']['provider'] == 'gce'
  secrets_path = if node['redis_exporter']['omnibus_secrets']
                   "gitlab-#{env}-secrets/gitlab-omnibus-secrets"
                 else
                   "gitlab-#{env}-secrets/redis-exporter"
                 end
  node.default['redis_exporter']['secrets'] = {
    backend: 'gkms',
    key: {
      ring: 'gitlab-secrets',
      key: env,
      location: 'global',
    },
    path: {
      path: secrets_path,
      item: "#{env}.enc",
    },
  }
end

vault = get_secrets(node['redis_exporter']['secrets']['backend'],
                    node['redis_exporter']['secrets']['path'],
                    node['redis_exporter']['secrets']['key'])

redis_env = {}
redis_env.merge!(node['redis_exporter']['env'])

if node['redis_exporter']['omnibus_secrets']
  redis_pw = vault['omnibus-gitlab']['gitlab_rb']['redis']['password']
  redis_env['REDIS_PASSWORD'] = redis_pw
else
  cluster_name = node['redis_exporter']['cluster_name']
  redis_env.merge!(vault['redis_exporter'][cluster_name]['env'])
end

include_recipe 'ark::default'

%w( curl tar bzip2 ).each do |pkg|
  package pkg
end

dir_name = ::File.basename(node['redis_exporter']['dir'])
dir_path = ::File.dirname(node['redis_exporter']['dir'])

ark dir_name do
  url node['redis_exporter']['binary_url']
  checksum node['redis_exporter']['checksum']
  version node['redis_exporter']['version']
  prefix_root Chef::Config['file_cache_path']
  path dir_path
  owner node['prometheus']['user']
  group node['prometheus']['group']
  action :put
  if node['redis_exporter']['systemd']
    notifies :restart, 'systemd_unit[redis_exporter.service]'
  else
    notifies :restart, 'runit_service[redis_exporter]'
  end
end

if node['redis_exporter']['systemd']
  systemd_env = redis_env.map do |var, value|
    "\"#{var}=#{value}\""
  end.join(' ')

  systemd_unit 'redis_exporter.service' do
    content(
      {
        Unit: {
          Description: 'Prometheus redis_exporter',
          Documentation: ['https://github.com/oliver006/redis_exporter'],
          After: 'network.target',
        },
        Service: {
          Type: 'simple',
          Environment: systemd_env,
          ExecStart: "#{node['redis_exporter']['dir']}/redis_exporter",
          KillMode: 'process',
          Restart: 'always',
          RestartSec: '5s',
          User: node['prometheus']['user'],
        },
        Install: {
          WantedBy: 'multi-user.target',
        },
      }
    )
    action [:create, :enable, :start]
  end
else
  directory node['redis_exporter']['log_dir'] do
    owner node['prometheus']['user']
    group node['prometheus']['group']
    mode '0755'
    recursive true
  end

  include_recipe 'runit::default'
  runit_service 'redis_exporter' do
    options(env: redis_env)
    default_logger true
    log_dir node['redis_exporter']['log_dir']
  end
end

if node['redis_exporter']['monitor_sentinel']
  # "grse" refers to gitlab redis sentinel exporter
  grse_executable = "#{dir_path}/gitlab-redis-sentinel-exporter"

  cookbook_file grse_executable do
    source 'gitlab-redis-sentinel-exporter'
    mode '0770'
  end

  cron 'gitlab redis sentinel exporter' do
    user 'root'
    minute '*'
    command "#{grse_executable} | sponge #{node['redis_exporter']['gitlab_metric_file']}"
  end
end
