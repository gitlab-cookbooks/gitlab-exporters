#
# Cookbook:: gitlab-monitoring
# Recipe:: default
#
# Copyright:: 2016, GitLab
#

user node['prometheus']['user'] do
  system true
  shell '/bin/false'
  home "/opt/#{node['prometheus']['user']}"
  not_if node['prometheus']['user'] == 'root'
end

directory '/var/log/prometheus' do
  owner node['prometheus']['user']
  group node['prometheus']['group']
  mode '0755'
  recursive true
end

if node['platform_version'].to_i > 20
  # runit-systemd provided by runit::default is an empty transitional package in 22.04.
  # install runit-run to provide the same functionality.
  package 'runit-run'
end

# needed, else the kitchen tests fail because package 'autogen' can not be found by the ark cookbook
apt_update 'update' do
end.run_action(:update) if platform_family?('debian') && Chef::Config[:file_cache_path].include?('kitchen')
