require 'yaml'
include_recipe 'gitlab-exporters::default'

# Fetch secrets from Chef Vault

blackbox_conf = get_secrets(node['blackbox_exporter']['secrets']['backend'],
                            node['blackbox_exporter']['secrets']['path'],
                            node['blackbox_exporter']['secrets']['key'])['gitlab-exporters']['blackbox_exporter']

directory node['blackbox_exporter']['dir'] do
  owner node['prometheus']['user']
  group node['prometheus']['group']
  mode '0755'
  recursive true
end

directory node['blackbox_exporter']['log_dir'] do
  owner node['prometheus']['user']
  group node['prometheus']['group']
  mode '0755'
  recursive true
end

include_recipe 'ark::default'

%w( curl tar bzip2 ).each do |pkg|
  package pkg
end

dir_name = ::File.basename(node['blackbox_exporter']['dir'])
dir_path = ::File.dirname(node['blackbox_exporter']['dir'])

# Work around a race condition in Chef 16+ where the :restart notification from `ark` stomps on the :restart from installing the runit_service
# Which ultimately causes both to fail. Instead, only notify :restart from ark when we're upgrading the service.
blackbox_notification_action = :nothing
if File.exist? node['blackbox_exporter']['binary']
  blackbox_version = Mixlib::ShellOut.new("#{node['blackbox_exporter']['binary']} --version 2>&1 | awk '/blackbox_exporter, version/ {print $3}'").run_command.stdout.strip
  if blackbox_version != node['blackbox_exporter']['version']
    Chef::Log.warn("Starting update of the blackbox exporter")
    blackbox_notification_action = :restart
  end
end

ark dir_name do
  url node['blackbox_exporter']['binary_url']
  checksum node['blackbox_exporter']['checksum']
  version node['blackbox_exporter']['version']
  prefix_root Chef::Config['file_cache_path']
  path dir_path
  owner node['prometheus']['user']
  group node['prometheus']['group']
  action :put
  notifies blackbox_notification_action, 'runit_service[blackbox_exporter]'
end

blackbox_module 'http_2xx' do
  prober node['blackbox_exporter']['modules']['http_2xx']['prober']
  http_method node['blackbox_exporter']['modules']['http_2xx']['http_method']
end

blackbox_module 'http_2xx_custom_ua' do
  prober node['blackbox_exporter']['modules']['http_2xx']['prober']
  http_method node['blackbox_exporter']['modules']['http_2xx']['http_method']
  http_headers node['blackbox_exporter']['modules']['http_2xx_custom_ua']['headers'].to_h
end

blackbox_module 'http_post_2xx' do
  prober node['blackbox_exporter']['modules']['http_post_2xx']['prober']
  http_method node['blackbox_exporter']['modules']['http_post_2xx']['http_method']
end

blackbox_module 'http_head_2xx' do
  prober node['blackbox_exporter']['modules']['http_head_2xx']['prober']
  http_method node['blackbox_exporter']['modules']['http_head_2xx']['http_method']
end

tls_upgrade_header = {
  'Location' => 'https://',
}

blackbox_module 'http_tls_upgrade' do
  prober 'http'
  http_valid_status_codes [301, 302, 303, 304, 307, 308]
  http_no_follow_redirects true
  http_fail_if_ssl true
  http_headers tls_upgrade_header
end

blackbox_module 'https_redirect' do
  prober 'http'
  http_valid_status_codes [301, 302, 303, 304, 307, 308]
  http_no_follow_redirects true
  http_fail_if_not_ssl true
  http_headers tls_upgrade_header
end

blackbox_module 'http_426' do
  prober 'http'
  http_valid_status_codes [426]
  http_no_follow_redirects true
  http_fail_if_not_ssl true
end

dev_auth_header = {
  'PRIVATE-TOKEN' => blackbox_conf['dev_auth_2xx'],
}
blackbox_module 'http_dev_gitlab_org_2xx' do
  prober node['blackbox_exporter']['modules']['http_dev_gitlab_org_2xx']['prober']
  http_method node['blackbox_exporter']['modules']['http_dev_gitlab_org_2xx']['http_method']
  http_headers dev_auth_header
end

prod_auth_header = {
  'PRIVATE-TOKEN' => blackbox_conf['prod_auth_2xx'],
}
blackbox_module 'http_gitlab_com_auth_2xx' do
  prober node['blackbox_exporter']['modules']['http_gitlab_com_auth_2xx']['prober']
  http_method node['blackbox_exporter']['modules']['http_gitlab_com_auth_2xx']['http_method']
  http_headers prod_auth_header
end

elastic_nonprod = {
  'Authorization' => "Basic #{blackbox_conf['elastic_nonprod']}",
  'Content-Type' => 'application/json',
}
blackbox_module 'http_elastic_nonprod_ilm' do
  prober node['blackbox_exporter']['modules']['elastic']['prober']
  http_method node['blackbox_exporter']['modules']['elastic']['http_method']
  http_fail_if_body_matches_regexp [node['blackbox_exporter']['modules']['elastic']['ilm_error_regex']]
  http_headers elastic_nonprod
end
blackbox_module 'http_elastic_nonprod_ilm_status' do
  prober node['blackbox_exporter']['modules']['elastic']['prober']
  http_method node['blackbox_exporter']['modules']['elastic']['http_method']
  http_fail_if_body_matches_regexp [node['blackbox_exporter']['modules']['elastic']['ilm_status_stopped_regex']]
  http_headers elastic_nonprod
end

elastic_prod = {
  'Authorization' => "Basic #{blackbox_conf['elastic_prod']}",
  'Content-Type' => 'application/json',
}
blackbox_module 'http_elastic_prod_ilm' do
  prober node['blackbox_exporter']['modules']['elastic']['prober']
  http_method node['blackbox_exporter']['modules']['elastic']['http_method']
  http_fail_if_body_matches_regexp [node['blackbox_exporter']['modules']['elastic']['ilm_error_regex']]
  http_headers elastic_prod
end
blackbox_module 'http_elastic_prod_ilm_status' do
  prober node['blackbox_exporter']['modules']['elastic']['prober']
  http_method node['blackbox_exporter']['modules']['elastic']['http_method']
  http_fail_if_body_matches_regexp [node['blackbox_exporter']['modules']['elastic']['ilm_status_stopped_regex']]
  http_headers elastic_prod
end
blackbox_module 'http_elastic_prod_health' do
  prober node['blackbox_exporter']['modules']['elastic']['prober']
  http_method node['blackbox_exporter']['modules']['elastic']['http_method']
  http_fail_if_body_not_matches_regexp [node['blackbox_exporter']['modules']['elastic']['cluster_health_regex']]
  http_headers elastic_prod
end

elastic_monitoring_es7 = {
  'Authorization' => "Basic #{blackbox_conf['elastic_monitoring_es7']}",
  'Content-Type' => 'application/json',
}
blackbox_module 'http_elastic_monitoring_es7_ilm' do
  prober node['blackbox_exporter']['modules']['elastic']['prober']
  http_method node['blackbox_exporter']['modules']['elastic']['http_method']
  http_fail_if_body_matches_regexp [node['blackbox_exporter']['modules']['elastic']['ilm_error_regex']]
  http_headers elastic_monitoring_es7
end
blackbox_module 'http_elastic_monitoring_es7_ilm_status' do
  prober node['blackbox_exporter']['modules']['elastic']['prober']
  http_method node['blackbox_exporter']['modules']['elastic']['http_method']
  http_fail_if_body_matches_regexp [node['blackbox_exporter']['modules']['elastic']['ilm_status_stopped_regex']]
  http_headers elastic_monitoring_es7
end
blackbox_module 'http_elastic_monitoring_es7_health' do
  prober node['blackbox_exporter']['modules']['elastic']['prober']
  http_method node['blackbox_exporter']['modules']['elastic']['http_method']
  http_fail_if_body_not_matches_regexp [node['blackbox_exporter']['modules']['elastic']['cluster_health_regex']]
  http_headers elastic_monitoring_es7
end

blackbox_module 'tcp_connect' do
  prober node['blackbox_exporter']['modules']['tcp_connect']['prober']
  timeout node['blackbox_exporter']['modules']['tcp_connect']['timeout']
end

blackbox_module 'pop3s_banner' do
  prober node['blackbox_exporter']['modules']['pop3s_banner']['prober']
  tcp_tls node['blackbox_exporter']['modules']['pop3s_banner']['tcp_tls']
  tcp_tls_config node['blackbox_exporter']['modules']['pop3s_banner']['tcp_tls_config']
  tcp_query_response node['blackbox_exporter']['modules']['pop3s_banner']['tcp_query_response']
end

blackbox_module 'icmp' do
  prober node['blackbox_exporter']['modules']['icmp']['prober']
  timeout node['blackbox_exporter']['modules']['icmp']['timeout']
end

ssh_banner = {
  'expect' => '^SSH-2.0-',
}
blackbox_module 'ssh_banner' do
  prober 'tcp'
  tcp_query_response ssh_banner
end

blackbox_module 'tls_expiry' do
  prober node['blackbox_exporter']['modules']['tls_expiry']['prober']
  timeout node['blackbox_exporter']['modules']['tls_expiry']['timeout']
  tcp_tls node['blackbox_exporter']['modules']['tls_expiry']['tcp_tls']
  tcp_tls_config node['blackbox_exporter']['modules']['tls_expiry']['tcp_tls_config']
end

template node['blackbox_exporter']['flags']['config.file'] do
  source 'blackbox_exporter.yml.erb'
  owner node['prometheus']['user']
  group node['prometheus']['group']
  mode '0644'
  notifies :hup, 'runit_service[blackbox_exporter]'
end

include_recipe 'runit::default'
runit_service 'blackbox_exporter' do
  default_logger true
  log_dir node['blackbox_exporter']['log_dir']
end

# Consul registration

if !node['gitlab_consul'].nil? && node['gitlab_consul']['agent']['enabled'] == true
  port = node['blackbox_exporter']['port']
  consul_definition 'blackbox_exporter' do
    type 'service'
    parameters(
      check: {
        http: "http://127.0.0.1:#{port}/",
        interval: '15s',
      },
      name: 'blackbox_exporter',
      port: port.to_i,
      tags: [
        'metrics',
      ]
    )
    notifies :reload, 'consul_service[consul]', :delayed
  end
end
