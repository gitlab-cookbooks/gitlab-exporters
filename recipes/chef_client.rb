require 'time'

include_recipe 'gitlab-exporters::node_exporter'

chef_gem 'prometheus-client' do
  compile_time true
  version '1.0.0'
end

handler = 'prometheus_handler.rb'
path    = File.join(Chef::Config['file_cache_path'], handler)
cookbook_file(path) do
  source handler
  mode '0755'
  action :nothing
end.run_action(:create)

textfile = File.join(node['node_exporter']['flags']['collector.textfile.directory'], 'chef-client.prom')

# Just in case we register the handler but crash before the first client run.
# Drop a metrics file that contains an error.
template textfile do
  source 'chef-client.prom.erb'
  owner node['prometheus']['user']
  group node['prometheus']['group']
  mode '0644'
  variables timestamp: Time.now.utc.to_f
  not_if { ::File.exist?(textfile) }
end

chef_handler('PrometheusHandler') do
  source path
  arguments textfile: textfile
  action :nothing
end.run_action(:enable)
