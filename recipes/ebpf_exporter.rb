require 'yaml'

include_recipe 'gitlab-exporters::default'
include_recipe 'gitlab-server::bcc'

directory node['ebpf_exporter']['dir'] do
  owner node['prometheus']['user']
  group node['prometheus']['group']
  mode '0755'
  recursive true
end

directory node['ebpf_exporter']['log_dir'] do
  owner node['prometheus']['user']
  group node['prometheus']['group']
  mode '0755'
  recursive true
end

include_recipe 'ark::default'

%w( curl tar ).each do |pkg|
  package pkg
end

dir_name = ::File.basename(node['ebpf_exporter']['dir'])
dir_path = ::File.dirname(node['ebpf_exporter']['dir'])

ark dir_name do
  url node['ebpf_exporter']['binary_url']
  checksum node['ebpf_exporter']['checksum']
  version node['ebpf_exporter']['version']
  prefix_root Chef::Config['file_cache_path']
  path dir_path
  owner node['prometheus']['user']
  group node['prometheus']['group']
  action :put
  notifies :restart, 'systemd_unit[ebpf_exporter.service]'
end

file 'ebpf_exporter config' do
  path node['ebpf_exporter']['config_file']
  owner node['prometheus']['user']
  group node['prometheus']['group']
  mode '0644'
  content lazy { hash_to_yaml(node['ebpf_exporter']['config']) }
  notifies :restart, 'systemd_unit[ebpf_exporter.service]', :delayed
end

ebpf_exporter_cmd = [
  node['ebpf_exporter']['binary'],
  Gitlab::Prometheus.kingpin_flags_for(node, 'ebpf_exporter'),
  ].join(' ')

ebpf_exporter_unit = {
  Unit: {
    Description: 'ebpf_exporter',
    Documentation: ['https://github.com/cloudflare/ebpf_exporter/'],
    After: 'network.target',
  },
  Service: {
    Type: 'simple',
    ExecStart: ebpf_exporter_cmd,
    KillMode: 'process',
    MemoryLimit: '200M',
    LimitNOFILE: '10000',
    Restart: 'always',
    RestartSec: '5s',
    User: 'root',
  },
  Install: {
    WantedBy: 'multi-user.target',
  },
}

kernel_version = node['kernel']['release']
ebpf_supported = Gem::Version.new(kernel_version) >= Gem::Version.new('4.10')

systemd_unit 'ebpf_exporter.service' do
  content ebpf_exporter_unit
  action ebpf_supported ? [:create, :enable, :start] : [:create, :disable, :stop]
end
