require 'yaml'
include_recipe 'gitlab-exporters::default'

directory node['statsd_exporter']['dir'] do
  owner node['prometheus']['user']
  group node['prometheus']['group']
  mode '0755'
  recursive true
end

directory node['statsd_exporter']['log_dir'] do
  owner node['prometheus']['user']
  group node['prometheus']['group']
  mode '0755'
  recursive true
end

include_recipe 'ark::default'

%w( curl tar ).each do |pkg|
  package pkg
end

dir_name = ::File.basename(node['statsd_exporter']['dir'])
dir_path = ::File.dirname(node['statsd_exporter']['dir'])

ark dir_name do
  url node['statsd_exporter']['binary_url']
  checksum node['statsd_exporter']['checksum']
  version node['statsd_exporter']['version']
  prefix_root Chef::Config['file_cache_path']
  path dir_path
  owner node['prometheus']['user']
  group node['prometheus']['group']
  action :put
  notifies :restart, 'systemd_unit[statsd_exporter.service]'
end

config_yaml = {
  defaults: node['statsd_exporter']['defaults'],
  mappings: node['statsd_exporter']['mappings'],
}

file 'statsd_exporter mappings' do
  path node['statsd_exporter']['mapping_file']
  owner node['prometheus']['user']
  group node['prometheus']['group']
  mode '0644'
  content hash_to_yaml(config_yaml)
  notifies :restart, 'systemd_unit[statsd_exporter.service]', :delayed
end

statsd_exporter_cmd = [
  node['statsd_exporter']['binary'],
  Gitlab::Prometheus.kingpin_flags_for(node, 'statsd_exporter'),
].join(' ')

statsd_exporter_unit = {
  Unit: {
    Description: 'Prometheus statsd_exporter',
    Documentation: ['https://github.com/prometheus/statsd_exporter'],
    After: 'network.target',
  },
  Service: {
    Type: 'simple',
    ExecStart: statsd_exporter_cmd,
    KillMode: 'process',
    MemoryLimit: '100M',
    LimitNOFILE: '10000',
    Restart: 'always',
    RestartSec: '5s',
    User: node['prometheus']['user'],
  },
  Install: {
    WantedBy: 'multi-user.target',
  },
}

systemd_unit 'statsd_exporter.service' do
  content statsd_exporter_unit
  action [:create, :enable, :start]
end

# Consul registration

if !node['gitlab_consul'].nil? && node['gitlab_consul']['agent']['enabled'] == true
  port = node['statsd_exporter']['port']
  consul_definition 'statsd_exporter' do
    type 'service'
    parameters(
      check: {
        http: "http://127.0.0.1:#{port}/",
        interval: '15s',
      },
      name: 'statsd_exporter',
      port: port.to_i,
      tags: [
        'metrics',
      ]
    )
    notifies :reload, 'consul_service[consul]', :delayed
  end
end
