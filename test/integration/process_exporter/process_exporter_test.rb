# InSpec tests

control 'groups-and-users' do
  impact 1.0
  title 'General group and user tests'

  describe etc_group do
    its('gids') { should_not contain_duplicates }
  end

  describe user('prometheus') do
    it { should exist }
  end
end

control 'process_exporter is properly installed' do
  impact 1.0
  describe file('/etc/process-exporter/chef-configured.yaml') do
    it { should exist }
    its('content') { should match(/name: git-{{.Matches.subcommand}}/) }
  end

  describe file('/etc/systemd/system/process-exporter.service.d/override.conf') do
    it { should exist }
  end

  describe package('process-exporter') do
    it { should be_installed }
  end

  describe service('process-exporter') do
    it { should be_installed }
    it { should be_enabled }
    it { should be_running }
  end
end
