# InSpec tests

control 'cadvisor binary is installed' do
  impact 1.0
  describe file('/opt/prometheus/cadvisor/cadvisor') do
    it { should exist }
  end
end

control 'cadvisor service is created with correct command flags and is enabled' do
  impact 1.0
  describe file('/etc/systemd/system/cadvisor.service') do
    its('content') { should match %r{ExecStart = /opt/prometheus/cadvisor/cadvisor --port=9101 --disable_metrics=percpu,cpu_topology,cpuLoad,cpuset,network} }
  end

  describe service('cadvisor') do
    it { should be_installed }
    it { should be_enabled }
    it { should be_running }
  end
end
