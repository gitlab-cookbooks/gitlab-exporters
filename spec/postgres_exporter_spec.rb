require 'spec_helper'
require 'chef-vault'
require 'chef-vault/test_fixtures'

describe 'gitlab-exporters::postgres_exporter' do
  include ChefVault::TestFixtures.rspec_shared_context

  context 'default execution' do
    cached(:chef_run) do
      ChefSpec::SoloRunner.new do |node|
      end.converge(described_recipe)
    end

    it 'creates the prometheus dir and plugin in the configured location' do
      expect(chef_run).to create_directory('/opt/prometheus/postgres_exporter').with(
        owner: 'prometheus',
        group: 'prometheus',
        mode: '0755',
        recursive: true
      )
    end

    it 'creates the query configuration file' do
      expect(chef_run).to create_template('/opt/prometheus/postgres_exporter/queries.yaml').with(
        owner: 'prometheus',
        group: 'prometheus',
        mode: '0644'
      )
      expect(chef_run).to render_file('/opt/prometheus/postgres_exporter/queries.yaml').with_content { |content|
        expect(content).to eq(IO.read('spec/fixtures/queries.yaml.template'))
      }
      expect(chef_run.template('/opt/prometheus/postgres_exporter/queries.yaml')).to notify('runit_service[postgres_exporter]').to(:reload).delayed
    end

    context 'Marginalia sampler' do
      RegexpTest = Struct.new(:input, :application, :endpoint_id, :database, :command)

      def marginalia_tests
        [
          RegexpTest.new(
            %(/*application:web,db_config_name:main*/ REINDEX INDEX CONCURRENTLY "public"."index_ci_builds_on_token_partial"),
            'web', nil, nil, 'REINDEX'
          ),
          RegexpTest.new(
            %(/*application:web,correlation_id:01FH892B2KDTNNJSAZ48ADT626,endpoint_id:Projects::IssuesController#show,db_config_name:main_replica*/ SELECT 1),
            'web', 'Projects::IssuesController#show', nil, 'SELECT'
          ),
          RegexpTest.new(%(autovacuum: VACUUM public.merge_request_diff_commits), nil, nil, nil, 'autovacuum'),
          RegexpTest.new(
            %(/*application:web,db_config_database:gitlabhq_production,db_config_name:main*/ REINDEX INDEX CONCURRENTLY "public"."index_ci_builds_on_token_partial"),
            'web', nil, 'gitlabhq_production', 'REINDEX'
          ),
          RegexpTest.new(
            %(/*application:web,correlation_id:01FH892B2KDTNNJSAZ48ADT626,endpoint_id:Projects::IssuesController#show,db_config_database:gitlabhq_production,db_config_name:main_replica*/ SELECT 1),
            'web', 'Projects::IssuesController#show', 'gitlabhq_production', 'SELECT'
          ),
        ]
      end

      def extract_regexp_from_sql(yaml, key)
        query = yaml[key]['query']
        data = query.match(/regexp_matches\(.*, '(.*)'\)/)

        return Regexp.new(data[1]) if data
      end

      it 'matches the regexp' do
        expect(chef_run).to render_file('/opt/prometheus/postgres_exporter/queries.yaml').with_content { |content|
          %w(pg_stat_activity_marginalia_sampler pg_long_running_transactions_marginalia).each do |key|
            yaml = YAML.safe_load(content)
            regex = extract_regexp_from_sql(yaml, key)

            expect(regex).not_to be_nil

            marginalia_tests.each do |test|
              data = test.input.match(regex)

              expect(data).not_to be_nil
              expect(data[1]).to eq(test.application)
              expect(data[2]).to eq(test.endpoint_id)
              expect(data[3]).to eq(test.database)
              expect(data[4]).to eq(test.command)
            end
          end
        }
      end
    end

    context 'pg_stat_kcache metrics on omnibus-gitlab' do
      let(:chef_run) do
        ChefSpec::SoloRunner.new do |node|
          node.normal['omnibus-gitlab']['gitlab_rb']['postgresql']['shared_preload_libraries'] = 'pg_stat_statements,pg_stat_kcache'
        end.converge(described_recipe)
      end

      it 'are enabled when pg_stat_kcache is present in node attributes' do
        expect(chef_run).to render_file('/opt/prometheus/postgres_exporter/queries.yaml').with_content { |content|
          yaml = YAML.safe_load(content)
          expect(yaml['pg_stat_kcache']).not_to be_nil
        }
      end
    end

    context 'pg_stat_kcache metrics on patroni servers' do
      let(:chef_run) do
        ChefSpec::SoloRunner.new do |node|
          node.normal['gitlab-patroni']['postgresql']['parameters']['shared_preload_libraries'] = 'pg_stat_statements,pg_stat_kcache'
        end.converge(described_recipe)
      end

      it 'are enabled when pg_stat_kcache is present in node attributes' do
        expect(chef_run).to render_file('/opt/prometheus/postgres_exporter/queries.yaml').with_content { |content|
          yaml = YAML.safe_load(content)
          expect(yaml['pg_stat_kcache']).not_to be_nil
        }
      end
    end

    context 'in main patroni roles' do
      let(:chef_run) do
        ChefSpec::SoloRunner.new do |node|
          node.normal['prometheus']['labels']['type'] = 'patroni'
        end.converge(described_recipe)
      end

      it 'creates the rails-specific queries' do
        expect(chef_run).to render_file('/opt/prometheus/postgres_exporter/queries.yaml').with_content { |content|
          expect(content).to eq(IO.read('spec/fixtures/queries-rails-main.yaml.template'))
        }
      end
    end

    context 'in ci patroni roles' do
      let(:chef_run) do
        ChefSpec::SoloRunner.new do |node|
          node.normal['prometheus']['labels']['type'] = 'patroni-ci'
        end.converge(described_recipe)
      end

      it 'creates the rails-specific queries' do
        expect(chef_run).to render_file('/opt/prometheus/postgres_exporter/queries.yaml').with_content { |content|
          expect(content).to eq(IO.read('spec/fixtures/queries-rails-ci.yaml.template'))
        }
      end
    end

    context 'in v14 patroni roles' do
      let(:chef_run) do
        ChefSpec::SoloRunner.new do |node|
          node.normal['gitlab-patroni']['postgresql']['version'] = '14'
        end.converge(described_recipe)
      end

      it 'creates the new v14 specific queries' do
        expect(chef_run).to render_file('/opt/prometheus/postgres_exporter/queries.yaml').with_content { |content|
          expect(content).to eq(IO.read('spec/fixtures/queries-v14.yaml.template'))
        }
      end
    end

    context 'in main patroni v14 roles' do
      let(:chef_run) do
        ChefSpec::SoloRunner.new do |node|
          node.normal['prometheus']['labels']['type'] = 'patroni'
          node.normal['gitlab-patroni']['postgresql']['version'] = '14'
        end.converge(described_recipe)
      end

      it 'creates the rails-specific queries' do
        expect(chef_run).to render_file('/opt/prometheus/postgres_exporter/queries.yaml').with_content { |content|
          expect(content).to eq(IO.read('spec/fixtures/queries-rails-main-v14.yaml.template'))
        }
      end
    end

    context 'in ci patroni v14 roles' do
      let(:chef_run) do
        ChefSpec::SoloRunner.new do |node|
          node.normal['prometheus']['labels']['type'] = 'patroni-ci'
          node.normal['gitlab-patroni']['postgresql']['version'] = '14'
        end.converge(described_recipe)
      end

      it 'creates the rails-specific queries' do
        expect(chef_run).to render_file('/opt/prometheus/postgres_exporter/queries.yaml').with_content { |content|
          expect(content).to eq(IO.read('spec/fixtures/queries-rails-ci-v14.yaml.template'))
        }
      end
    end

    it 'creates the log dir in the configured location' do
      expect(chef_run).to create_directory('/var/log/prometheus/postgres_exporter').with(
        owner: 'prometheus',
        group: 'prometheus',
        mode: '0755',
        recursive: true
      )
    end

    it 'creates the runit service' do
      expect(chef_run).to enable_runit_service('postgres_exporter')
    end
  end
end
