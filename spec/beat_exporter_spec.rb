require 'spec_helper'
require 'chef-vault'
require 'chef-vault/test_fixtures'

describe 'gitlab-exporters::beat_exporter' do
  include ChefVault::TestFixtures.rspec_shared_context

  context 'default execution' do
    cached(:chef_run) do
      ChefSpec::SoloRunner.new do |node|
      end.converge(described_recipe)
    end

    it 'includes the `ark` recipe' do
      expect(chef_run).to include_recipe('ark::default')
    end

    it 'installs beat_exporter' do
      expect(chef_run).to dump_ark('beat-exporter')
    end

    it 'enables beat_exporter service' do
      expect(chef_run).to create_systemd_unit('beat_exporter.service')
      expect(chef_run).to enable_systemd_unit('beat_exporter.service')
      expect(chef_run).to start_systemd_unit('beat_exporter.service')
    end
  end
end
