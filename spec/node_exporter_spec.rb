require 'spec_helper'
require 'chefspec'

gce_disks_prom_content = <<-GCE_DISKS_PROM
# HELP node_disk_max_read_iops Maximum number of read IOs per second
# TYPE node_disk_max_read_iops gauge
node_disk_max_read_iops{device="sda"} 600.0
node_disk_max_read_iops{device="sdb"} 15.0
# HELP node_disk_max_write_iops Maximum number of write IOs per second
# TYPE node_disk_max_write_iops gauge
node_disk_max_write_iops{device="sda"} 600.0
node_disk_max_write_iops{device="sdb"} 30.0
# HELP node_disk_max_read_bytes_seconds Maximum number of read per second
# TYPE node_disk_max_read_bytes_seconds gauge
node_disk_max_read_bytes_seconds{device="sda"} 10066329.6
node_disk_max_read_bytes_seconds{device="sdb"} 2516582.4
# HELP node_disk_max_write_bytes_seconds Maximum number of write per second
# TYPE node_disk_max_write_bytes_seconds gauge
node_disk_max_write_bytes_seconds{device="sda"} 10066329.6
node_disk_max_write_bytes_seconds{device="sdb"} 2516582.4
GCE_DISKS_PROM

describe 'gitlab-exporters::node_exporter' do
  context 'default execution' do
    cached(:chef_run) do
      ChefSpec::SoloRunner.new(path: 'spec/fixtures/gce_16.04.json') do |node|
      end.converge('consul::default', described_recipe)
    end

    it 'creates the prometheus dir in the configured location' do
      expect(chef_run).to create_directory('/opt/prometheus/node_exporter').with(
        owner: 'prometheus',
        group: 'prometheus',
        mode: '0755',
        recursive: true
      )
    end

    it 'creates the log dir in the configured location' do
      expect(chef_run).to create_directory('/var/log/prometheus/node_exporter').with(
        owner: 'prometheus',
        group: 'prometheus',
        mode: '0755',
        recursive: true
      )
    end

    it 'creates the runit service' do
      expect(chef_run).to enable_runit_service('node_exporter')
    end

    it 'creates scripts directory' do
      expect(chef_run).to create_directory('/opt/prometheus/node_exporter/scripts').with(
          owner: 'prometheus',
          group: 'prometheus',
          mode: '0755'
        )
    end

    it 'creates textfile metrics directory' do
      expect(chef_run).to create_directory('/opt/prometheus/node_exporter/metrics').with(
          owner: 'prometheus',
          group: 'prometheus',
          mode: '0755'
        )
    end

    it 'creates a gce disk text file' do
      expect(chef_run).to render_file('/opt/prometheus/node_exporter/metrics/gce_disk_io_quota.prom').with_content(gce_disks_prom_content)
    end
  end

  context 'ntpd metrics' do
    cached(:chef_run) do
      ChefSpec::SoloRunner.new do |node|
      end.converge('consul::default', described_recipe)
    end

    it 'creates the ntpd_metrics.py script' do
      expect(chef_run).to render_file('/opt/prometheus/node_exporter/ntpd_metrics.py')
    end

    it 'creates the log dir in the configured location' do
      expect(chef_run).to create_directory('/var/log/prometheus/node_exporter_ntpd_metrics').with(
        owner: 'prometheus',
        group: 'prometheus',
        mode: '0755',
        recursive: true
      )
    end

    it 'creates the runit service' do
      expect(chef_run).to enable_runit_service('ntpd_metrics')
    end
  end

  context 'apt metrics' do
    cached(:chef_run) do
      ChefSpec::SoloRunner.new do |node|
      end.converge('consul::default', described_recipe)
    end

    it 'creates the apt.sh script' do
      expect(chef_run).to render_file('/opt/prometheus/node_exporter/apt.sh')
    end

    it 'creates the log dir in the configured location' do
      expect(chef_run).to create_directory('/var/log/prometheus/node_exporter_apt_metrics').with(
        owner: 'prometheus',
        group: 'prometheus',
        mode: '0755',
        recursive: true
      )
    end

    it 'creates the runit service' do
      expect(chef_run).to enable_runit_service('apt_metrics')
    end
  end
end
