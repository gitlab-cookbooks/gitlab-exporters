require 'spec_helper'

describe 'gitlab-exporters::chef_client' do
  context 'default execution' do
    cached(:chef_run) do
      ChefSpec::ServerRunner.new do |node|
      end.converge(described_recipe)
    end

    it 'Runs Successfully' do
      expect { chef_run }.to_not raise_error
    end

    it 'Installs gems' do
      expect(chef_run).to install_chef_gem('prometheus-client').with(
        compile_time: true
      )
    end

    it 'creates an error metric file' do
      expect(chef_run).to create_template('/opt/prometheus/node_exporter/metrics/chef-client.prom').with(
        owner: 'prometheus',
        group: 'prometheus',
        mode: '0644'
      )
      expect(chef_run).to render_file('/opt/prometheus/node_exporter/metrics/chef-client.prom').with_content { |content|
        expect(content).to match(/^chef_client_last_run_timestamp_seconds \d+\.\d+$/)
        expect(content).to match(/^chef_client_error 1.0$/)
      }
    end

    it 'uploads the handler' do
      expect(chef_run).to create_cookbook_file(File.join(ENV['TMPDIR'] || Chef::Config['file_cache_path'], 'prometheus_handler.rb')).with(
        mode: '0755'
      )
    end

    it 'does its thing' do
      expect(chef_run).to enable_chef_handler('PrometheusHandler').with(
        source: File.join(ENV['TMPDIR'] || Chef::Config['file_cache_path'], 'prometheus_handler.rb'),
        arguments: { textfile: '/opt/prometheus/node_exporter/metrics/chef-client.prom' }
      )
    end
  end
end
