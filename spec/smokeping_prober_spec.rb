require 'spec_helper'
require 'chef-vault'
require 'chef-vault/test_fixtures'

setcap_cmd = 'setcap cap_net_raw+ep /opt/prometheus/smokeping_prober/smokeping_prober'
setcap_verify_cmd = 'setcap -v cap_net_raw+ep /opt/prometheus/smokeping_prober/smokeping_prober'

describe 'gitlab-exporters::smokeping_prober' do
  include ChefVault::TestFixtures.rspec_shared_context

  context 'default execution' do
    before do
      # Stub that the capability has not yet been set.
      stub_command(setcap_verify_cmd).and_return(false)
    end
    cached(:chef_run) do
      ChefSpec::SoloRunner.new do |node|
      end.converge(described_recipe)
    end

    it 'creates the binary dir in the configured location' do
      expect(chef_run).to create_directory('/opt/prometheus/smokeping_prober').with(
        owner: 'prometheus',
        group: 'prometheus',
        mode: '0755',
        recursive: true
      )
    end

    it 'creates the log dir in the configured location' do
      expect(chef_run).to create_directory('/var/log/prometheus/smokeping_prober').with(
        owner: 'prometheus',
        group: 'prometheus',
        mode: '0755',
        recursive: true
      )
    end

    it 'installs packages' do
      expect(chef_run).to install_package('curl')
      expect(chef_run).to install_package('tar')
      expect(chef_run).to install_package('bzip2')
      expect(chef_run).to install_package('libcap2-bin')
    end

    it 'installs the smokeping_prober binary' do
      expect(chef_run).to put_ark('smokeping_prober')
    end

    it 'sets capabilities on the binary' do
      expect(chef_run).to run_execute(setcap_cmd)
    end

    it 'creates the systemd service unit' do
      expect(chef_run).to create_systemd_unit('smokeping_prober.service')
      expect(chef_run).to enable_systemd_unit('smokeping_prober.service')
      expect(chef_run).to start_systemd_unit('smokeping_prober.service')
    end
  end
end
