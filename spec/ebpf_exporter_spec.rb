require 'spec_helper'
require 'chef-vault'
require 'chef-vault/test_fixtures'

config = <<~EOS
  ---
  programs:
  - name: timers
    metrics:
      counters:
      - name: timer_start_total
        help: Timers fired in the kernel
        table: counts
        labels:
        - name: function
          size: 8
          decoders:
          - name: ksym
    tracepoints:
      timer:timer_start: tracepoint__timer__timer_start
    code: |-
      BPF_HASH(counts, u64);
      // Generates function tracepoint__timer__timer_start
      TRACEPOINT_PROBE(timer, timer_start) {
          counts.increment((u64) args->function);
          return 0;
      }
EOS

describe 'gitlab-exporters::ebpf_exporter' do
  include ChefVault::TestFixtures.rspec_shared_context

  context 'default execution' do
    let(:kernel_version) { '5.4.0-100-generic' }
    let(:ebpf_config) do
      {
        "programs": [
          {
            "name": 'timers',
            "metrics": {
              "counters": [
                {
                  "name": 'timer_start_total',
                  "help": 'Timers fired in the kernel',
                  "table": 'counts',
                  "labels": [
                    {
                      "name": 'function',
                      "size": 8,
                      "decoders": [
                        {
                          "name": 'ksym',
                        },
                      ],
                    },
                  ],
                },
              ],
            },
            "tracepoints": {
              "timer:timer_start": 'tracepoint__timer__timer_start',
            },
            "code": "BPF_HASH(counts, u64);\n// Generates function tracepoint__timer__timer_start\nTRACEPOINT_PROBE(timer, timer_start) {\n    counts.increment((u64) args->function);\n    return 0;\n}",
          },
        ],
      }
    end

    cached(:chef_run) do
      ChefSpec::SoloRunner.new do |node|
        node.automatic['kernel']['release'] = kernel_version
        node.normal['ebpf_exporter']['config'] = ebpf_config
      end.converge(described_recipe)
    end

    it 'creates the binary dir in the configured location' do
      expect(chef_run).to create_directory('/opt/prometheus/ebpf_exporter').with(
        owner: 'prometheus',
        group: 'prometheus',
        mode: '0755',
        recursive: true
      )
    end

    it 'creates the log dir in the configured location' do
      expect(chef_run).to create_directory('/var/log/prometheus/ebpf_exporter').with(
        owner: 'prometheus',
        group: 'prometheus',
        mode: '0755',
        recursive: true
      )
    end

    it 'installs packages' do
      expect(chef_run).to install_package('curl')
      expect(chef_run).to install_package('tar')
    end

    it 'installs the ebpf_exporter binary' do
      expect(chef_run).to put_ark('ebpf_exporter')
    end

    it 'creates a ebpf mapping config' do
      expect(chef_run).to create_file('/opt/prometheus/ebpf_exporter/config.yml').with(
        owner: 'prometheus',
        group: 'prometheus',
        mode: '0644'
      )
      expect(chef_run).to render_file('/opt/prometheus/ebpf_exporter/config.yml').with_content { |content|
                            expect(content).to eq(config)
                          }
    end

    it 'creates the systemd service unit' do
      expect(chef_run).to create_systemd_unit('ebpf_exporter.service')
      expect(chef_run).to enable_systemd_unit('ebpf_exporter.service')
      expect(chef_run).to start_systemd_unit('ebpf_exporter.service')
    end

    context 'with a 4.4 kernel' do
      let(:kernel_version) { '4.4.0-218-generic' }
      cached(:chef_run) do
        ChefSpec::SoloRunner.new do |node|
          node.automatic['kernel']['release'] = kernel_version
          node.normal['ebpf_exporter']['config'] = ebpf_config
        end.converge(described_recipe)
      end

      it 'disables the systemd service unit' do
        expect(chef_run).to create_systemd_unit('ebpf_exporter.service')
        expect(chef_run).to disable_systemd_unit('ebpf_exporter.service')
        expect(chef_run).to stop_systemd_unit('ebpf_exporter.service')
      end
    end
  end
end
