require 'spec_helper'
require 'chef-vault'
require 'chef-vault/test_fixtures'

describe 'gitlab-exporters::blackbox_exporter' do
  include ChefVault::TestFixtures.rspec_shared_context

  context 'default execution' do
    cached(:chef_run) do
      ChefSpec::SoloRunner.new(step_into: ['blackbox_module']) do |node|
        node.normal['gitlab-exporters']['blackbox_exporter']['chef_vault'] = 'blackbox_exporter'
      end.converge(described_recipe)
    end

    it 'creates the blackbox_exporter dir in the configured location' do
      expect(chef_run).to create_directory('/opt/prometheus/blackbox_exporter').with(
        owner: 'prometheus',
        group: 'prometheus',
        mode: '0755',
        recursive: true
      )
    end

    it 'creates the log dir in the configured location' do
      expect(chef_run).to create_directory('/var/log/prometheus/blackbox_exporter').with(
        owner: 'prometheus',
        group: 'prometheus',
        mode: '0755',
        recursive: true
      )
    end

    it 'creates the configuration file with default content' do
      expect(chef_run).to create_template('/opt/prometheus/blackbox_exporter/blackbox_exporter.yml').with(
        owner: 'prometheus',
        group: 'prometheus',
        mode: '0644'
      )
      expect(chef_run).to render_file('/opt/prometheus/blackbox_exporter/blackbox_exporter.yml').with_content { |content|
        expect(content).to include('modules:')
      }
    end

    it 'renders blackbox exporter config' do
      expect(chef_run).to render_file('/opt/prometheus/blackbox_exporter/blackbox_exporter.yml').with_content { |content|
                            expect(content).to eq(<<-eos
---
modules:
  http_2xx:
    prober: http
    timeout: 59s
    http:
      preferred_ip_protocol: ip4
  http_2xx_custom_ua:
    prober: http
    timeout: 59s
    http:
      preferred_ip_protocol: ip4
      headers:
        user-agent: gitlab-blackbox-probe
  http_post_2xx:
    prober: http
    timeout: 59s
    http:
      preferred_ip_protocol: ip4
      method: POST
  http_head_2xx:
    prober: http
    timeout: 59s
    http:
      preferred_ip_protocol: ip4
      method: HEAD
  http_tls_upgrade:
    prober: http
    timeout: 59s
    http:
      preferred_ip_protocol: ip4
      no_follow_redirects: true
      fail_if_ssl: true
      valid_status_codes:
      - 301
      - 302
      - 303
      - 304
      - 307
      - 308
      headers: &1
        Location: https://
  https_redirect:
    prober: http
    timeout: 59s
    http:
      preferred_ip_protocol: ip4
      no_follow_redirects: true
      fail_if_not_ssl: true
      valid_status_codes:
      - 301
      - 302
      - 303
      - 304
      - 307
      - 308
      headers: *1
  http_426:
    prober: http
    timeout: 59s
    http:
      preferred_ip_protocol: ip4
      no_follow_redirects: true
      fail_if_not_ssl: true
      valid_status_codes:
      - 426
  http_dev_gitlab_org_2xx:
    prober: http
    timeout: 59s
    http:
      preferred_ip_protocol: ip4
      method: GET
      headers:
        PRIVATE-TOKEN: dummy-password
  http_gitlab_com_auth_2xx:
    prober: http
    timeout: 59s
    http:
      preferred_ip_protocol: ip4
      method: GET
      headers:
        PRIVATE-TOKEN: dummy-password
  http_elastic_nonprod_ilm:
    prober: http
    timeout: 59s
    http:
      preferred_ip_protocol: ip4
      method: GET
      fail_if_body_matches_regexp:
      - '"step":"ERROR"'
      headers: &2
        Authorization: 'Basic '
        Content-Type: application/json
  http_elastic_nonprod_ilm_status:
    prober: http
    timeout: 59s
    http:
      preferred_ip_protocol: ip4
      method: GET
      fail_if_body_matches_regexp:
      - '"operation_mode":"STOPPED"'
      headers: *2
  http_elastic_prod_ilm:
    prober: http
    timeout: 59s
    http:
      preferred_ip_protocol: ip4
      method: GET
      fail_if_body_matches_regexp:
      - '"step":"ERROR"'
      headers: &3
        Authorization: 'Basic '
        Content-Type: application/json
  http_elastic_prod_ilm_status:
    prober: http
    timeout: 59s
    http:
      preferred_ip_protocol: ip4
      method: GET
      fail_if_body_matches_regexp:
      - '"operation_mode":"STOPPED"'
      headers: *3
  http_elastic_prod_health:
    prober: http
    timeout: 59s
    http:
      preferred_ip_protocol: ip4
      method: GET
      fail_if_body_not_matches_regexp:
      - '"status":"green"'
      headers: *3
  http_elastic_monitoring_es7_ilm:
    prober: http
    timeout: 59s
    http:
      preferred_ip_protocol: ip4
      method: GET
      fail_if_body_matches_regexp:
      - '"step":"ERROR"'
      headers: &4
        Authorization: 'Basic '
        Content-Type: application/json
  http_elastic_monitoring_es7_ilm_status:
    prober: http
    timeout: 59s
    http:
      preferred_ip_protocol: ip4
      method: GET
      fail_if_body_matches_regexp:
      - '"operation_mode":"STOPPED"'
      headers: *4
  http_elastic_monitoring_es7_health:
    prober: http
    timeout: 59s
    http:
      preferred_ip_protocol: ip4
      method: GET
      fail_if_body_not_matches_regexp:
      - '"status":"green"'
      headers: *4
  tcp_connect:
    prober: tcp
    timeout: 5s
    tcp:
      preferred_ip_protocol: ip4
      tls: false
  pop3s_banner:
    prober: tcp
    timeout: 59s
    tcp:
      preferred_ip_protocol: ip4
      query_response:
      - expect: "^+OK"
      tls: true
      tls_config:
        insecure_skip_verify: false
  icmp:
    prober: icmp
    timeout: 5s
  ssh_banner:
    prober: tcp
    timeout: 59s
    tcp:
      preferred_ip_protocol: ip4
      query_response:
      - expect: "^SSH-2.0-"
      tls: false
  tls_expiry:
    prober: tcp
    timeout: 59s
    tcp:
      preferred_ip_protocol: ip4
      tls: true
      tls_config:
        insecure_skip_verify: false

eos
                                                 )
                          }
    end

    it 'installs the blackbox_exporter binary' do
      expect(chef_run).to put_ark('blackbox_exporter')
    end

    it 'creates the runit service' do
      expect(chef_run).to enable_runit_service('blackbox_exporter')
    end
  end
end
