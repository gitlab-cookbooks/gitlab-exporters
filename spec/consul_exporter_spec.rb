require 'spec_helper'
require 'chef-vault'
require 'chef-vault/test_fixtures'

describe 'gitlab-exporters::consul_exporter' do
  include ChefVault::TestFixtures.rspec_shared_context

  context 'default execution' do
    cached(:chef_run) do
      ChefSpec::SoloRunner.new do |node|
      end.converge(described_recipe)
    end

    it 'creates the prometheus dir and plugin in the configured location' do
      expect(chef_run).to create_directory('/opt/prometheus/consul_exporter').with(
        owner: 'prometheus',
        group: 'prometheus',
        mode: '0755',
        recursive: true
      )
    end

    it 'creates the log dir in the configured location' do
      expect(chef_run).to create_directory('/var/log/prometheus/consul_exporter').with(
        owner: 'prometheus',
        group: 'prometheus',
        mode: '0755',
        recursive: true
      )
    end

    it 'creates the runit service' do
      expect(chef_run).to enable_runit_service('consul_exporter')
    end
  end
end
