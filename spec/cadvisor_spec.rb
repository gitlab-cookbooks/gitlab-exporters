require 'spec_helper'
require 'chef-vault'
require 'chef-vault/test_fixtures'

describe 'gitlab-exporters::cadvisor' do
  include ChefVault::TestFixtures.rspec_shared_context

  context 'default execution' do
    cached(:chef_run) do
      ChefSpec::SoloRunner.new.converge(described_recipe)
    end

    it 'creates the binary dir in the configured location' do
      expect(chef_run).to create_directory('/opt/prometheus/cadvisor').with(
        owner: 'prometheus',
        group: 'prometheus',
        mode: '0755',
        recursive: true
      )
    end

    it 'creates the systemd service unit' do
      expect(chef_run).to create_systemd_unit('cadvisor.service')
      expect(chef_run).to enable_systemd_unit('cadvisor.service')
      expect(chef_run).to start_systemd_unit('cadvisor.service')
    end
  end
end
